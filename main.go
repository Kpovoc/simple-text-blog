package main

import (
		"gitlab.com/Kpovoc/simple-text-blog/src/handlers"
  "net/http"
  "log"
)

func main() {
	handlers.Init()
  err := http.ListenAndServe(":8080", nil)
  if err != nil {
    log.Fatalln(err)
  }
}