package handlers

import (
  "io/ioutil"
  "log"
  "strings"
  "sort"
  "html/template"
  "regexp"
  "net/http"
)

var latestPage *Page
var postsPage *Page

type PostLink struct {
  Title string;
  Date string;
  Filename string;
}
var postsValidPath = regexp.MustCompile("^/(posts)/([a-zA-Z0-9\\-]+(.html))")

func latestHandler(w http.ResponseWriter, r *http.Request) {
  renderPage(w, latestPage)
}

func postsPageHandler(w http.ResponseWriter, r *http.Request) {
  renderPage(w, postsPage)
}

func postsHandler(w http.ResponseWriter, r *http.Request) {
  m := postsValidPath.FindStringSubmatch(r.URL.Path)
  if m == nil {
    http.NotFound(w, r)
    return
  }

  page, err := createPostPage(m[2])
  if err != nil {
    http.Error(w, err.Error(), http.StatusInternalServerError)
    return
  }

  renderPage(w, page)
}

func initializePosts() {
  files, err := ioutil.ReadDir("resources/posts")
  if err != nil {
    log.Fatalln(err)
  }

  links := []PostLink{}

  for _, f := range files {
    file, err := ioutil.ReadFile("resources/posts/" + f.Name())
    if err != nil {
      log.Fatalln(err)
    }
    content := string(file)
    h2BegInd := strings.Index(content, "<h2>") + 4
    h2EndInd := strings.Index(content, "</h2>")
    h3BegInd := strings.Index(content, "<h3>") + 4
    h3EndInd := strings.Index(content, "</h3>")
    title := content[h2BegInd:h2EndInd]
    date := content[h3BegInd:h3EndInd]
    links = append(links, PostLink{
      Title: title,
      Date: date,
      Filename: f.Name(),
    })
  }

  sort.Slice(links, func(i, j int) bool {
    return links[i].Date > links[j].Date
  })

  sb := strings.Builder{}

  for _, pl := range links {
    sb.WriteString(`<a href="/posts/`)
    sb.WriteString(pl.Filename)
    sb.WriteString(`">`)
    sb.WriteString(pl.Date)
    sb.WriteString(` `)
    sb.WriteString(pl.Title)
    sb.WriteString(`</a><br>`)
  }

  latestPage, err = createPostPage(links[0].Filename)
  if err != nil {
    log.Fatalln(err)
  }

  postsPage = &Page{
    Title: "Posts",
    Content: template.HTML(sb.String()),
    Footer: template.HTML(
      `<span class="navitem">&copy; 2018 KhronoSync Digital Solutions LLC</span>`),
  }
}

func createPostPage(filename string) (*Page, error) {
  file, err := ioutil.ReadFile("resources/posts/" + filename)
  content := string(file)
  h2BegInd := strings.Index(content, "<h2>") + 4
  h2EndInd := strings.Index(content, "</h2>")
  return &Page {
    Title: content[h2BegInd:h2EndInd],
    Content: template.HTML(content),
    Footer: template.HTML(
      `<span class="navitem">&copy; 2018 KhronoSync Digital Solutions LLC</span>`),
  }, err
}