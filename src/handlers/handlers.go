package handlers

import (
  "net/http"
  "html/template"
)

var templates *template.Template

type Page struct {
  Title string
  Content template.HTML;
  Footer template.HTML;
}

type Footer struct {
  Previous string;
  Next string;
}

func Init() {
  tmplRoot := "resources/templates/"

  // template.Must is a convenience wrapper that panics when passed a non-nil error value, and
  // otherwise returns the *Template unaltered. A panic is appropriate here; if the templates
  // can't be loaded the only sensible thing to do is exit the program.
  templates = template.Must(
    template.ParseFiles(
      tmplRoot + "page.html"))

  initializePosts()

  http.HandleFunc("/", latestHandler)
  http.HandleFunc("/posts", postsPageHandler)
  http.HandleFunc("/posts/", postsHandler)
  http.HandleFunc("/projects", projectsHandler)
  http.HandleFunc("/contact", contactHandler)
}

func projectsHandler(w http.ResponseWriter, r *http.Request) {
  content := template.HTML(`
<a href="https://gitlab.com/Kpovoc/chat-steward">Chat Steward</a><br>
<a href="https://gitlab.com/Kpovoc/simple-text-blog">Simple Text Blog</a><br>
`)

  renderPage(w, &Page{
    Title: "Projects",
    Content: content,
    Footer: template.HTML(
      `<span>&copy; 2018 KhronoSync Digital Solutions LLC</span>`),
  })
}

func contactHandler(w http.ResponseWriter, r *http.Request) {
  content := template.HTML(`
<ul>
  <li>Twitter: <a href="https://twitter.com/KpovocKDS">@KpovocKDS</a></li>
  <li>Telegram: @Kpovoc</li>
</ul>
`)

  renderPage(w, &Page{
    Title: "Contact Us",
    Content: content,
    Footer: template.HTML(
      `<span class="navitem">&copy; 2018 KhronoSync Digital Solutions LLC</span>`),
  })
}

func renderPage(w http.ResponseWriter, p *Page) {
  err := templates.ExecuteTemplate(w, "page.html", p)
  if nil != err {
    http.Error(w, err.Error(), http.StatusInternalServerError)
  }
}
